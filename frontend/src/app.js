import { renderRoutes } from 'react-router-config'
import routes from './routes'
import GlobalStyle from './base/global_style'

const Main = styled.main`
    display: flex;
    flex-direction: column;
    min-height: 100vh;
`

export default class App extends React.Component {
    render() {
        return (
            <Main>
                <GlobalStyle />
                {renderRoutes(routes)}
            </Main>
        )
    }
}
