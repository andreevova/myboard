import { Redirect } from 'react-router'
import PrivatePage from './components/private_page'
import HomePage from './components/home_page'
import BoardPage from './components/board_page'

export default [
    {
        path: '/',
        exact: true,
        component: HomePage
    },
    {
        path: '/board/:uid',
        component: props => (
            <PrivatePage {...props}>
                <BoardPage {...props} />
            </PrivatePage>
        )
    },
    {
        path: '/board',
        component: () => <Redirect to="/" />
    }
]
