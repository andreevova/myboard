import Query from './query'
import openSocket from 'socket.io-client'

const api = 'http://localhost:8080/api'
const socket = openSocket('http://localhost:8080')

export default {
    board: (way, params) => new Query(api, `/board${way}`, params),
    column: (way, params) => new Query(api, `/column${way}`, params),
    card: (way, params) => new Query(api, `/card${way}`, params),
    socket: {
        emit: (...data) => socket.emit(...data),
        on: (...data) => socket.on(...data)
    }
}
