import Logger from './logger'

export default class Query {
    constructor(api, path, params) {
        this.api = api
        this.path = path
        this.params = params
        this.run()
    }

    logger = new Logger()

    run() {
        this.promise = fetch(this.getFetchUrl(), this.getFetchOptions())
            .then(response => response.json())
            .then(data => {
                if (data.success) {
                    this.logger.query(this.path, data)
                    return data.data
                } else {
                    throw data
                }
            })
            .catch(err => {
                this.logger.error(this.path, err)
                throw err
            })
    }

    getFetchUrl() {
        return `${this.api}${this.path}`
    }

    getFetchOptions() {
        return {
            method: 'post',
            headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
            body: this.params ? JSON.stringify(this.params) : undefined
        }
    }

    then(...args) {
        return this.promise.then(...args)
    }

    catch(...args) {
        return this.promise.catch(...args)
    }
}
