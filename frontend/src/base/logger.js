const NODE_ENV = process.env.NODE_ENV

export default class Logger {
    query(url, data) {
        if (NODE_ENV === 'production') return
        console.groupCollapsed(`%c ${url}`, 'background: #00DB8E; padding-right: 5px; color: #fff; border-radius: 4px;')
        console.log(JSON.stringify(data, null, 4))
        console.groupEnd()
    }

    error(url, data) {
        if (NODE_ENV === 'production') return
        console.groupCollapsed(`%c ${url}`, 'background: #FF006E; padding-right: 5px; color: #fff; border-radius: 4px;')
        console.log(JSON.stringify(data, null, 4))
        console.groupEnd()
    }
}
