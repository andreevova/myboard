import { BoardStore } from 'stores'
import { Element, Form, ButtonSubmit } from './column_edit.styled'

@observer
export default class ColumnEditPopup extends React.Component {
    static propTypes = {
        data: PropTypes.shape({
            uid: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired
        }).isRequired
    }

    static contextTypes = {
        setLoading: PropTypes.func.isRequired
    }

    handleSubmit = ({ name }) => {
        this.context.setLoading(true)

        Api.column('/edit', { board: this.props.data.uid, column: this.props.data.id, name })
            .then(() => BoardStore.request(this.props.data.uid))
            .catch(({ msg }) => {
                if (msg === 'name_required') alert('Введите название колонки')
                else if (msg === 'name_exists') alert('Название существует')
                else alert('Неизвестная ошибка')
                this.form.checkInputs()
            })
            .then(this.context.setLoading(false))
    }

    handleFormRef = e => (this.form = e)

    render() {
        let { name } = this.props.data

        return (
            <Element>
                <Form onSubmit={this.handleSubmit} ref={this.handleFormRef}>
                    <UIInputText
                        type="text"
                        name="name"
                        placeholder="Название колонки"
                        value={name}
                        autoFocus
                        center
                        point
                    />
                    <ButtonSubmit type="submit">Изменить</ButtonSubmit>
                </Form>
            </Element>
        )
    }
}
