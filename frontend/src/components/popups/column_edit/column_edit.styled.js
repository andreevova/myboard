export const Element = styled.div``

export const Form = styled(UIForm)`
    display: flex;
    flex-direction: column;
`

export const ButtonSubmit = styled.button`
    margin-top: 15px;
    background: ${COLOR.GREEN};
    border: none;
    border-radius: 8px;
    padding: 8px 16px;
    font-size: 13px;
    color: #fff;
    cursor: pointer;
    transition: box-shadow 0.3s ease, transform 0.3s ease;

    :hover {
        transform: translateY(-2px);
        box-shadow: 0 5px 30px ${rgba(COLOR.GREEN, 0.5)}, 0 2px 5px ${rgba(COLOR.GREEN, 0.7)};
    }
`
