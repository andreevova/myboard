import { BoardStore } from 'stores'
import { Element, Title, Form, InputText, ButtonSubmit } from './card_add.styled'

@observer
export default class CardAddPopup extends React.Component {
    static propTypes = {
        data: PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired
        }).isRequired
    }

    static contextTypes = {
        setLoading: PropTypes.func.isRequired
    }

    handleSubmit = ({ name, text }) => {
        this.context.setLoading(true)

        Api.card('/add', { board: BoardStore.data.board.uid, column: this.props.data.id, name, text })
            .then(() => BoardStore.request(BoardStore.data.board.uid))
            .then(() => this.form.reset())
            .catch(({ msg }) => {
                if (msg === 'name_required') alert('Введите название карточки')
                else if (msg === 'text_required') alert('Введите текст')
                else if (msg === 'name_exists') alert('Название существует')
                else alert('Неизвестная ошибка')
                this.form.checkInputs()
            })
            .then(this.context.setLoading(false))
    }

    handleFormRef = e => (this.form = e)

    render() {
        let { name } = this.props.data

        return (
            <Element>
                <Title>Добавление карточки в «{name}»</Title>
                <Form onSubmit={this.handleSubmit} ref={this.handleFormRef}>
                    <InputText type="text" name="name" placeholder="Название карточки" autoFocus center point />
                    <InputText type="text" name="text" placeholder="Текст" center point />
                    <ButtonSubmit type="submit">Добавить</ButtonSubmit>
                </Form>
            </Element>
        )
    }
}
