import { BoardStore } from 'stores'
import { Element, Form, ButtonSubmit } from './column_add.styled'

@observer
export default class ColumnAddPopup extends React.Component {
    static contextTypes = {
        setLoading: PropTypes.func.isRequired
    }

    handleSubmit = ({ name }) => {
        this.context.setLoading(true)

        Api.column('/add', { board: BoardStore.data.board.uid, name })
            // .then(data => {
            //     let columns = BoardStore.data.columns
            //     columns.push({ ...data, cards: [] })
            //     BoardStore.data.columns = columns
            // })
            .then(this.form.reset())
            .catch(({ msg }) => {
                if (msg === 'name_required') alert('Введите название колонки')
                else if (msg === 'name_exists') alert('Колонка существует')
                else alert('Неизвестная ошибка')
                this.form.checkInputs()
            })
            .then(this.context.setLoading(false))
    }

    handleFormRef = e => (this.form = e)

    render() {
        return (
            <Element>
                <Form onSubmit={this.handleSubmit} ref={this.handleFormRef}>
                    <UIInputText type="text" name="name" placeholder="Название колонки" autoFocus center point />
                    <ButtonSubmit type="submit">Добавить</ButtonSubmit>
                </Form>
            </Element>
        )
    }
}
