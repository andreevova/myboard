export const Element = styled.div`
    display: flex;
    flex-direction: column;
`

export const Title = styled.p`
    font-size: 16px;
    margin-bottom: 30px;
    color: ${COLOR.SILVER};
`

export const Text = styled.p``

export const History = styled.div`
    background: ${rgba(COLOR.BG_BLACK, 0.03)};
    border-radius: 8px;
    font-size: 12px;
    padding: 15px;
    text-align: center;
    margin-top: 30px;
`

export const HistoryTitle = styled.b`
    display: block;
    color: ${COLOR.SILVER};
    margin-bottom: 10px;
`

export const Item = styled.div`
    color: ${COLOR.SILVER};
    margin-bottom: 10px;

    :last-child {
        margin-bottom: 0;
    }
`
