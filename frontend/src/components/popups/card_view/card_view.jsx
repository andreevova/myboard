import { BoardStore } from 'stores'
import { Element, Title, Text, History, HistoryTitle, Item } from './card_view.styled'

@observer
export default class CardViewPopup extends React.Component {
    static propTypes = {
        data: PropTypes.shape({
            id: PropTypes.string.isRequired,
            column: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
            history: PropTypes.arrayOf(
                PropTypes.shape({
                    fromColumn: PropTypes.string.isRequired,
                    toColumn: PropTypes.string.isRequired,
                    fromIndex: PropTypes.number.isRequired,
                    toIndex: PropTypes.number.isRequired,
                    transfer: PropTypes.string.isRequired
                }).isRequired
            ).isRequired
        }).isRequired
    }

    render() {
        let { name, text, history } = this.props.data

        return (
            <Element>
                <Title>{name}</Title>
                <Text>{text}</Text>
                {!!history.length && (
                    <History>
                        <HistoryTitle>История перемещений</HistoryTitle>
                        {history.map(({ fromColumn, toColumn, fromIndex, toIndex }, index) => {
                            let { columns } = BoardStore.data

                            let fromColumnName = !columns.filter(x => x.id === fromColumn).length
                                ? undefined
                                : columns.filter(x => x.id === fromColumn)[0]['name']
                            let toColumnName = !columns.filter(x => x.id === toColumn).length
                                ? undefined
                                : columns.filter(x => x.id === toColumn)[0]['name']

                            return (
                                fromColumnName &&
                                toColumnName && (
                                    <Item key={index}>
                                        {fromColumnName} ({fromIndex + 1}) => {toColumnName} ({toIndex + 1})
                                    </Item>
                                )
                            )
                        })}
                    </History>
                )}
            </Element>
        )
    }
}
