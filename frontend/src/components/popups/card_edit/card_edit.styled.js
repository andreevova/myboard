export const Element = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const Title = styled.p`
    font-size: 16px;
    margin-bottom: 30px;
    color: ${COLOR.SILVER};
`

export const Form = styled(UIForm)`
    display: flex;
    flex-direction: column;
    width: 100%;
`

export const InputText = styled(UIInputText)`
    margin-bottom: 10px;
`

export const ButtonSubmit = styled.button`
    background: ${COLOR.GREEN};
    border: none;
    border-radius: 8px;
    padding: 8px 16px;
    margin-top: 5px;
    font-size: 13px;
    color: #fff;
    cursor: pointer;
    transition: box-shadow 0.3s ease, transform 0.3s ease;

    :hover {
        transform: translateY(-2px);
        box-shadow: 0 5px 30px ${rgba(COLOR.GREEN, 0.5)}, 0 2px 5px ${rgba(COLOR.GREEN, 0.7)};
    }
`
