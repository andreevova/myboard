import { BoardStore } from 'stores'
import { Element, Title, Form, InputText, ButtonSubmit } from './card_edit.styled'

@observer
export default class CardEditPopup extends React.Component {
    static propTypes = {
        data: PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
            column: PropTypes.string.isRequired
        }).isRequired
    }

    static contextTypes = {
        setLoading: PropTypes.func.isRequired
    }

    handleSubmit = ({ name, text }) => {
        this.context.setLoading(true)

        Api.card('/edit', {
            board: BoardStore.data.board.uid,
            column: this.props.data.column,
            card: this.props.data.id,
            name,
            text
        })
            .then(() => BoardStore.request(BoardStore.data.board.uid))
            .catch(({ msg }) => {
                if (msg === 'name_required') alert('Введите название карточки')
                else if (msg === 'text_required') alert('Введите текст')
                else if (msg === 'name_exists') alert('Название существует')
                else alert('Неизвестная ошибка')
                this.form.checkInputs()
            })
            .then(this.context.setLoading(false))
    }

    handleFormRef = e => (this.form = e)

    render() {
        let { name, text } = this.props.data

        return (
            <Element>
                <Title>Редактирование карточки</Title>
                <Form onSubmit={this.handleSubmit} ref={this.handleFormRef}>
                    <InputText
                        type="text"
                        name="name"
                        placeholder="Название карточки"
                        value={name}
                        autoFocus
                        center
                        point
                    />
                    <InputText type="text" name="text" placeholder="Текст" value={text} center point />
                    <ButtonSubmit type="submit">Изменить</ButtonSubmit>
                </Form>
            </Element>
        )
    }
}
