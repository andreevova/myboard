import PopupStore from 'stores/popup'
import Main from './main'
import { Element } from './popup.styled'

@observer
export default class UIPopup extends React.Component {
    render() {
        let { popups } = PopupStore

        if (!popups.length) return null

        return (
            <Element>
                {popups.map(({ size, content }, index) => (
                    <Main key={index} size={size} show={index + 1 === popups.length ? 1 : 0}>
                        {content}
                    </Main>
                ))}
            </Element>
        )
    }
}
