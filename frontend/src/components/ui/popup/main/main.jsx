import PopupStore from 'stores/popup'
import { Element, Close, CloseIcon } from './main.styled'
import closeIcon from './img/close.svg'

@observer
export default class Main extends React.Component {
    UNSAFE_componentWillMount() {
        document.addEventListener('click', this.handleClickOutside)
        document.addEventListener('keyup', this.handleKeyUp)
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside)
        document.addEventListener('keyup', this.handleKeyUp)
    }

    handleClickOutside = () => {
        const domNode = ReactDOM.findDOMNode(this)
        if (!domNode || !domNode.contains(event.target)) PopupStore.close()
    }

    handleKeyUp = e => e.keyCode === 27 && PopupStore.close()

    render() {
        let { children, size, show } = this.props

        return (
            <Element {...{ size, show }}>
                <Close onClick={() => PopupStore.close()}>
                    <CloseIcon src={closeIcon} size={12} />
                </Close>
                {children}
            </Element>
        )
    }
}
