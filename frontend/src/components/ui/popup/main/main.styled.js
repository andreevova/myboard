const fadeInUp = keyframes`
    from {
        opacity: 0;
        transform: translateY(50px);
    }

    to {
        opacity: 1;
        transform: translateY(0);
    }
`

export const Element = styled.div`
    position: relative;
    width: calc(100% - 120px);
    max-width: calc(${props => (props.size ? props.size : '400')}px - 60px);
    background: #fff;
    border-radius: 8px;
    box-shadow: 0 2px 2px ${rgba(COLOR.BLACK, 0.03)}, 0 4px 4px ${rgba(COLOR.BLACK, 0.03)},
        0 8px 8px ${rgba(COLOR.BLACK, 0.03)};
    animation-duration: 0.3s;
    animation-fill-mode: both;
    animation-name: ${fadeInUp};
    padding: 30px;
    margin: 100px auto;
    color: ${COLOR.BLACK};
    display: ${props => (props.show ? 'block' : 'none')};
`

export const Close = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    display: flex;
    padding: 10px;
    cursor: pointer;
    opacity: 0.3;
    transition: all 0.2s ease-in-out;
    z-index: 1;

    :hover {
        opacity: 1;
    }
`

export const CloseIcon = styled(UISvgIcon)`
    fill: ${COLOR.BLACK};
`
