const fadeIn = keyframes`
    from {
        opacity: 0;
    }

    to {
        opacity: 1;
    }
`

export const Element = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    background: ${rgba(COLOR.BLACK, 0.5)};
    animation-duration: 0.3s;
    animation-fill-mode: both;
    animation-name: ${fadeIn};
    overflow-y: auto;
    z-index: 9999;
`
