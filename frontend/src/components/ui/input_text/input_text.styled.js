export const Element = styled.input`
    height: 42px;
    width: 100%;
    padding: 0 16px;
    border-radius: 8px;
    border: 1px solid ${p => (p.err ? rgba(COLOR.RED, 0.3) : rgba(COLOR.BG_BLACK, 0.05))};
    text-align: ${p => (p.center ? 'center' : 'left')};
    transition: border 0.3s ease;

    ::placeholder {
        color: ${p => (p.err ? COLOR.RED : COLOR.SILVER)};
        font-size: 13px;
        transition: color 0.3s ease;
    }

    :focus {
        border: 1px solid ${rgba(COLOR.BG_BLACK, 0.1)};

        ::placeholder {
            color: ${rgba(COLOR.SILVER, 0.5)};
        }
    }
`
