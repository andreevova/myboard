import { Element } from './input_text.styled'

class InputTextState extends AbstractState {
    @observable value = ''
    @observable err = false
}

@observer
export default class InputText extends React.Component {
    state = new InputTextState()

    static propTypes = {
        type: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        placeholder: PropTypes.string,
        center: PropTypes.bool,
        noForm: PropTypes.bool,
        point: PropTypes.bool,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        autoFocus: PropTypes.bool
    }

    static defaultProps = {
        onChange: () => {},
        noForm: false,
        point: false,
        autoFocus: false
    }

    static contextTypes = {
        registerInput: PropTypes.func,
        unregisterInput: PropTypes.func
    }

    @action
    reset = () => this.state.set({ value: '' })

    @action
    checkErr = () => {
        if (this.props.point) {
            if (!this.state.value) this.state.set({ err: true })
        }
    }

    UNSAFE_componentWillMount() {
        if (this.props.value) this.state.set({ value: this.props.value })
    }

    componentDidMount() {
        if (this.context.registerInput && !this.props.noForm) this.context.registerInput(this)
    }

    componentWillUnmount() {
        if (this.context.unregisterInput && !this.props.noForm) this.context.unregisterInput(this)
    }

    handleChange = e => {
        this.state.set({ err: false, value: e.target.value })
        this.props.onChange(e)
    }

    render() {
        let { type, name, placeholder, point, ...other } = this.props,
            { value, err } = this.state

        return (
            <Element
                {...{ type, name, placeholder, point, err, ...other }}
                value={value}
                autoComplete="off"
                onChange={this.handleChange}
            />
        )
    }
}
