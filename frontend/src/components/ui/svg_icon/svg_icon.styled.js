export const Element = styled.svg`
    display: inline-block;
    width: ${props => props.size}px;
    height: ${props => props.size}px;
    fill: ${COLOR.BLACK};
    vertical-align: middle;
`
