import { Element } from './svg_icon.styled'

const CACHE = {}

class UISvgIconState extends AbstractState {
    @observable innerHTML
}

@observer
export default class UISvgIcon extends React.Component {
    state = new UISvgIconState()

    static resolve(arg) {
        return typeof arg === 'string' ? React.createElement(this, { src: arg }) : arg
    }

    static propTypes = {
        src: PropTypes.string,
        size: PropTypes.number,
        onClick: PropTypes.func
    }

    static defaultProps = {
        size: 24,
        onClick: () => {}
    }

    UNSAFE_componentWillMount() {
        this.loadSvg(this.props.src)
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (this.props.src !== nextProps.src) this.loadSvg(nextProps.src)
    }

    loadSvg(src) {
        Promise.resolve()
            .then(() => {
                return CACHE[src]
                    ? CACHE[src]
                    : fetch(src)
                          .then(result => result.text())
                          .then(text => (CACHE[src] = text.match(/<svg[\s\S]*?>([\s\S]+?)<\/svg>/)[1]))
            })
            .catch(e => console.warn(`Invalid icon at src ${src}`, e.stack))
            .then(innerHTML => this.state.set({ innerHTML }))
    }

    render() {
        let { className, size, onClick } = this.props,
            { innerHTML } = this.state

        return (
            <Element
                xmlns="http://www.w3.org/2000/svg"
                viewBox={`0 0 24 24`}
                {...{ size, className, onClick }}
                dangerouslySetInnerHTML={{ __html: innerHTML }}
            />
        )
    }
}
