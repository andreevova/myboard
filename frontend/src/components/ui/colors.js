export default {
    BG_LIGHT: '#F6F8FA',
    BG_BLACK: '#001540',
    BLACK: '#282B3C',
    BLUE: '#1076F6',
    SILVER: '#697494',
    GREEN: '#61D693',
    RED: '#EE5E6D',
    YELLOW: '#F9C546'
}
