import { ElementForm, ElementDiv } from './form.styled'

class FormState {
    @observable inputs = []
}

@observer
export default class Form extends React.Component {
    state = new FormState()

    static propTypes = {
        onSubmit: PropTypes.func
    }

    static defaultProps = {
        onSubmit: () => {}
    }

    static childContextTypes = {
        registerInput: PropTypes.func.isRequired,
        unregisterInput: PropTypes.func.isRequired
    }

    getChildContext() {
        let { registerInput, unregisterInput } = this
        return { registerInput, unregisterInput }
    }

    @action
    registerInput = input => this.state.inputs.push(input)

    @action
    unregisterInput = input => this.state.inputs.remove(input)

    @computed
    get value() {
        let data = {}

        this.state.inputs.forEach(({ state: { value, checked }, props: { name, type } }) => {
            if (type === 'checkbox') data[name] = checked
            else data[name] = value
        })

        return data
    }

    checkInputs() {
        this.state.inputs.forEach(({ checkErr, props: { point } }) => {
            if (point) checkErr()
        })
    }

    @action
    reset = () => this.state.inputs.forEach(input => input.reset())

    handleSubmit = e => {
        e.preventDefault()
        this.props.onSubmit(this.value)
    }

    render() {
        let { children, onSubmit, ...other } = this.props,
            Element = onSubmit ? ElementForm : ElementDiv

        return (
            <Element onSubmit={this.handleSubmit} {...other}>
                {children}
            </Element>
        )
    }
}
