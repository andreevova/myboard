import { Element, Start, Button, Form, InputText, ButtonSubmit, Boards, Link } from './home_page.styled'

class HomeState extends AbstractState {
    @observable visibleForm = false
}

@observer
export default class HomePage extends React.Component {
    state = new HomeState()

    handleClickButton = () => this.state.set({ visibleForm: true })

    handleSubmit = ({ name }) => {
        Api.board('/check', { name })
            .then(({ uid, name }) => {
                if (localStorage.getItem('boards')) {
                    let boards = JSON.parse(localStorage.getItem('boards'))
                    boards.push({ uid, name })
                    localStorage.setItem('boards', JSON.stringify(boards))
                } else {
                    localStorage.setItem('boards', JSON.stringify([{ uid, name }]))
                }
                return uid
            })
            .then(uid => this.props.history.push(`/board/${uid}`))
            .catch(() => this.form.checkInputs())
    }

    handleFormRef = e => (this.form = e)

    render() {
        let { visibleForm } = this.state,
            boards = localStorage.getItem('boards') ? JSON.parse(localStorage.getItem('boards')) : []

        return (
            <Element>
                {!visibleForm ? (
                    <Start>
                        <Button onClick={() => this.handleClickButton()}>Создать доску</Button>
                        {!!boards.length && (
                            <Boards key="boards">
                                {boards.reverse().map(({ uid, name }) => (
                                    <Link to={`/board/${uid}`} key={uid}>
                                        {name}
                                    </Link>
                                ))}
                            </Boards>
                        )}
                    </Start>
                ) : (
                    <Form onSubmit={this.handleSubmit} ref={this.handleFormRef}>
                        <InputText type="text" name="name" placeholder="Название доски" center point />
                        <ButtonSubmit type="submit">Создать</ButtonSubmit>
                    </Form>
                )}
            </Element>
        )
    }
}
