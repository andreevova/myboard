export const Element = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    min-height: 100vh;
`

export const Start = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const Button = styled.div`
    color: #fff;
    background: ${COLOR.BG_BLACK};
    padding: 16px 32px;
    font-size: 16px;
    border-radius: 100px;
    cursor: pointer;
    transition: box-shadow 0.3s ease, transform 0.3s ease;

    :hover {
        transform: translateY(-3px);
        box-shadow: 0 5px 30px ${rgba(COLOR.BG_BLACK, 0.5)}, 0 2px 5px ${rgba(COLOR.BG_BLACK, 0.7)};
    }
`

const fadeInUp = keyframes`
    from {
        transform: translateY(50px);
        opacity: 0;
    }

    to {
        transform: translateY(0);
        opacity: 1;
    }
`

export const Form = styled(UIForm)`
    display: flex;
    flex-direction: column;
    width: calc(100% - 30px);
    max-width: 320px;
    padding: 30px;
    background: #fff;
    border-radius: 8px;
    box-shadow: 0 5px 30px ${rgba(COLOR.BG_BLACK, 0.05)}, 0 2px 5px ${rgba(COLOR.BG_BLACK, 0.02)};
    animation-name: ${fadeInUp};
    animation-duration: 0.3s;
    animation-fill-mode: both;
`

export const InputText = styled(UIInputText)``

export const ButtonSubmit = styled.button`
    margin-top: 15px;
    background: ${COLOR.GREEN};
    border: none;
    border-radius: 8px;
    padding: 8px 16px;
    font-size: 13px;
    color: #fff;
    cursor: pointer;
    transition: box-shadow 0.3s ease, transform 0.3s ease;

    :hover {
        transform: translateY(-2px);
        box-shadow: 0 5px 30px ${rgba(COLOR.GREEN, 0.5)}, 0 2px 5px ${rgba(COLOR.GREEN, 0.7)};
    }
`

export const Boards = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 30px;
    padding-top: 30px;
    border-top: 1px solid ${rgba(COLOR.BLACK, 0.1)};
`

export const Link = styled(UILink)`
    font-weight: 500;
    margin-bottom: 5px;
    transition: color 0.3s ease;

    :hover {
        color: ${COLOR.BLUE};
    }

    :last-child {
        margin-bottom: 0;
    }
`
