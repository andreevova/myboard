import { BoardStore } from 'stores'
import { Redirect } from 'react-router'

class PrivatePageState extends AbstractState {
    @observable element = null
}

@observer
export default class PrivatePage extends React.Component {
    state = new PrivatePageState()

    UNSAFE_componentWillMount = () => {
        BoardStore.request(this.props.match.params.uid)
            .then(() => this.state.set({ element: this.props.children }))
            .then(() => {
                let boardData = BoardStore.data
                let boardId = boardData.board.uid

                Api.socket.on('columnTransfer', ({ board, column, fromIndex, toIndex }) => {
                    if (boardId === board) {
                        if (boardData.columns[toIndex].id === column) return

                        let to = boardData.columns

                        let [removed] = to.splice(fromIndex, 1)
                        to.splice(toIndex, 0, removed)

                        boardData.columns = to.filter(x => x !== undefined && x !== null)
                    }
                })

                Api.socket.on('columnEdit', ({ board, column, data }) => {
                    if (boardId === board) {
                        let indexColumn = boardData.columns.findIndex(x => x.id === column)
                        boardData.columns[indexColumn].name = data.name
                        boardData.columns[indexColumn].nameLower = data.nameLower
                    }
                })

                Api.socket.on('columnDelete', ({ board, column }) => {
                    if (boardId === board) {
                        let indexColumn = boardData.columns.findIndex(x => x.id === column)
                        boardData.columns.splice(indexColumn, 1)
                    }
                })

                Api.socket.on('columnAdd', ({ board, data }) => {
                    if (boardId === board) {
                        data.cards = []
                        boardData.columns.push({ ...data })
                    }
                })

                Api.socket.on('cardTransfer', ({ board, data }) => {
                    if (boardId === board) {
                        let newArrayColumns = []

                        boardData.columns.map(column => {
                            let cards = data.filter(card => card.column === column.id)
                            column.cards = cards
                            newArrayColumns.push({ ...column })
                        })

                        boardData.columns = newArrayColumns
                    }
                })

                Api.socket.on('cardEdit', ({ board, column, card, data }) => {
                    if (boardId === board) {
                        let indexColumn = boardData.columns.findIndex(x => x.id === column)
                        let indexCard = boardData.columns[indexColumn].cards.findIndex(x => x.id === card)

                        boardData.columns[indexColumn].cards[indexCard] = { ...data }
                    }
                })

                Api.socket.on('cardDelete', ({ board, column, card }) => {
                    if (boardId === board) {
                        let indexColumn = boardData.columns.findIndex(x => x.id === column)
                        let indexCard = boardData.columns[indexColumn].cards.findIndex(x => x.id === card)
                        boardData.columns[indexColumn].cards.splice(indexCard, 1)
                    }
                })

                Api.socket.on('cardAdd', ({ board, column, data }) => {
                    if (boardId === board) {
                        let indexColumn = boardData.columns.findIndex(x => x.id === column)
                        boardData.columns[indexColumn].cards.push({ ...data })
                    }
                })
            })
            .catch(() => this.state.set({ element: <Redirect to="/" /> }))
    }

    render() {
        let { element } = this.state
        return element
    }
}
