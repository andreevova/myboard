export const Element = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    height: 50px;
`

export const Side = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`

export const IconWrap = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 50px;
    height: 50px;
    background: ${COLOR.BLACK};
    border-radius: 8px;
    cursor: pointer;
    box-shadow: 0 5px 30px ${rgba(COLOR.BLACK, 0.3)};
    transition: box-shadow 0.3s ease;

    :hover {
        box-shadow: 0 5px 30px ${rgba(COLOR.BLACK, 0.8)};
    }
`

export const Icon = styled(UISvgIcon)`
    fill: #fff;
`

export const Information = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 15px;
`

export const Name = styled.p`
    font-size: 16px;
`

export const Created = styled.p`
    color: ${COLOR.SILVER};
    margin-top: 3px;
    font-size: 12px;
`

const loaderAnimated = keyframes`
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(360deg);
    }
`

export const Loader = styled.div`
    width: 30px;
    min-width: 30px;
    height: 30px;
    min-height: 30px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: auto;
    border-radius: 50%;
    border-top: 3px solid ${rgba(COLOR.SILVER, 0.2)};
    border-right: 3px solid ${rgba(COLOR.SILVER, 0.2)};
    border-bottom: 3px solid ${rgba(COLOR.SILVER, 0.2)};
    border-left: 3px solid ${COLOR.SILVER};
    transform: translateZ(0);
    animation: ${loaderAnimated} 0.3s infinite linear;
    opacity: ${p => (p.loading ? '1' : '0')};
    visibility: ${p => (p.loading ? 'visible' : 'hidden')};
    transition: opacity 0.3s ease, visibility 0.3s ease;
`
