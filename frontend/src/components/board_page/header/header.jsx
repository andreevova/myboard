import { PopupStore } from 'stores'
import { ColumnAddPopup } from 'popups'
import { Element, Side, IconWrap, Icon, Information, Name, Created, Loader } from './header.styled'
import plusIcon from './img/plus.svg'

@observer
export default class Header extends React.Component {
    static propTypes = {
        data: PropTypes.object.isRequired,
        loading: PropTypes.bool.isRequired
    }

    render() {
        let {
            data: { name, created },
            loading
        } = this.props

        return (
            <Element>
                <Side>
                    <IconWrap onClick={() => PopupStore.open(<ColumnAddPopup />)}>
                        <Icon src={plusIcon} size={16} />
                    </IconWrap>
                    <Information>
                        <Name>{name}</Name>
                        <Created>{moment(created).format('MMMM Do YYYY, H:mm')}</Created>
                    </Information>
                </Side>
                <Side>
                    <Loader loading={loading} />
                </Side>
            </Element>
        )
    }
}
