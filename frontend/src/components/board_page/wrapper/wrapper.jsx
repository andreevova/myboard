import { Droppable, Draggable } from 'react-beautiful-dnd'
import Column from './column'
import { Element, Block } from './wrapper.styled'

@observer
export default class Wrapper extends React.Component {
    static propTypes = {
        data: PropTypes.array.isRequired,
        providedBoard: PropTypes.object.isRequired
    }

    render() {
        let { data, providedBoard } = this.props

        return (
            <Element ref={providedBoard.innerRef} {...providedBoard.droppableProps}>
                {data.map(({ id, name, cards }, index) => (
                    <Draggable key={id} draggableId={id} index={index}>
                        {(provided, snapshot) => (
                            <Block
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                isDragging={snapshot.isDragging}
                            >
                                <Droppable droppableId={id} type="PERSON">
                                    {provided => <Column data={{ id, name, cards }} provided={provided} />}
                                </Droppable>
                            </Block>
                        )}
                    </Draggable>
                ))}
                {providedBoard.placeholder}
            </Element>
        )
    }
}
