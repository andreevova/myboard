export const Element = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-items: flex-start;
    flex: 1;
    margin-top: 30px;
    overflow: auto;
`

export const Block = styled.div`
    background: #eef0f4;
    box-shadow: ${p => (p.isDragging ? `0 0 0 5px ${COLOR.BG_LIGHT}` : 'none')};
    border-radius: 8px;
    margin: 0 15px;
    min-width: 320px;
    max-height: calc(100vh - 100px - 80px);
    overflow: auto;

    :first-child {
        margin: 0 15px 0 0;
    }
`
