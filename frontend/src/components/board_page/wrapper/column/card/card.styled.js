export const Element = styled.div`
    position: relative;
    background: #fff;
    border: 2px solid ${p => rgba(COLOR.SILVER, p.isDragging ? 0.2 : 0.1)};
    font-size: 13px;
    padding: 15px;
    margin: 0 15px 15px;
    border-radius: 8px;
    transition: background 0.3s ease, border 0.3s ease;

    :hover {
        border: 2px solid ${rgba(COLOR.SILVER, 0.2)};
    }
`

export const Title = styled.p`
    font-size: 14px;
    line-height: 1.4;
`

export const Settings = styled.div`
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-end;
    margin-top: 15px;
    z-index: 2;
    pointer-events: none;
`

export const Action = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 20px;
    height: 20px;
    border-radius: 8px;
    background: ${rgba(COLOR.SILVER, 0.2)};
    margin-right: 5px;
    pointer-events: all;
    transition: background 0.3s ease, transform 0.3s ease, box-shadow 0.3s ease;
    cursor: pointer;

    :first-child {
        :hover {
            background: ${COLOR.YELLOW};
            box-shadow: 0 5px 30px ${rgba(COLOR.YELLOW, 0.5)}, 0 2px 5px ${rgba(COLOR.YELLOW, 0.7)};
        }
    }

    :last-child {
        margin-right: 0;

        :hover {
            background: ${COLOR.RED};
            box-shadow: 0 5px 30px ${rgba(COLOR.RED, 0.5)}, 0 2px 5px ${rgba(COLOR.RED, 0.7)};
        }
    }

    :hover {
        transform: translateY(-2px);
        background: ${COLOR.BLACK};
        box-shadow: 0 5px 30px ${rgba(COLOR.BLACK, 0.5)}, 0 2px 5px ${rgba(COLOR.BLACK, 0.7)};
    }
`

export const Icon = styled(UISvgIcon)`
    fill: #fff;
`

export const Link = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 1;
    cursor: pointer;
`
