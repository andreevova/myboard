import { Draggable } from 'react-beautiful-dnd'
import { BoardStore, PopupStore } from 'stores'
import { CardEditPopup, CardViewPopup } from 'popups'
import { Element, Title, Settings, Action, Icon, Link } from './card.styled'
import editIcon from './img/edit.svg'
import trashIcon from './img/trash.svg'

@observer
export default class Card extends React.Component {
    static propTypes = {
        draggableId: PropTypes.string.isRequired,
        index: PropTypes.number.isRequired,
        data: PropTypes.object.isRequired
    }

    static contextTypes = {
        setLoading: PropTypes.func.isRequired
    }

    handleDelete = (board, column, card) => {
        this.context.setLoading(true)

        Api.card('/delete', { board, column, card })
            .then(() => BoardStore.request(BoardStore.data.board.uid))
            .catch(() => alert('Неизвестная ошибка'))
            .then(this.context.setLoading(false))
    }

    render() {
        let { draggableId, index, data } = this.props

        return (
            <Draggable draggableId={draggableId} index={index}>
                {(provided, snapshot) => (
                    <Element
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        isDragging={snapshot.isDragging}
                    >
                        <Title>{data.name}</Title>
                        <Settings>
                            <Action onClick={() => PopupStore.open(<CardEditPopup data={data} />)}>
                                <Icon src={editIcon} size={8} />
                            </Action>
                            <Action onClick={() => this.handleDelete(BoardStore.data.board.uid, data.column, data.id)}>
                                <Icon src={trashIcon} size={8} />
                            </Action>
                        </Settings>
                        <Link onClick={() => PopupStore.open(<CardViewPopup data={data} />)} />
                    </Element>
                )}
            </Draggable>
        )
    }
}
