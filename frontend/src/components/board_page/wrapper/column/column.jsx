import Header from './header'
import Card from './card'
import { Element } from './column.styled'

@observer
export default class Column extends React.Component {
    static propTypes = {
        data: PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            cards: PropTypes.array.isRequired
        }).isRequired,
        provided: PropTypes.object.isRequired
    }

    render() {
        let { data, provided } = this.props

        return (
            <Element ref={provided.innerRef} {...provided.droppableProps}>
                <Header data={data} />
                {data.cards.map(({ id, column, name, text, history }, index) => (
                    <Card key={id} draggableId={id} index={index} data={{ id, column, name, text, history }} />
                ))}
                {provided.placeholder}
            </Element>
        )
    }
}
