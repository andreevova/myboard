import { BoardStore, PopupStore } from 'stores'
import { ColumnEditPopup, CardAddPopup } from 'popups'
import { Element, Title, Menu, Icon } from './header.styled'
import trashIcon from './img/trash.svg'
import editIcon from './img/edit.svg'
import plusIcon from './img/plus.svg'

@observer
export default class Header extends React.Component {
    static propTypes = {
        data: PropTypes.shape({
            id: PropTypes.string.isRequired,
            name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
        }).isRequired
    }

    static contextTypes = {
        setLoading: PropTypes.func.isRequired
    }

    handleDelete = column => {
        this.context.setLoading(true)

        Api.column('/delete', { board: BoardStore.data.board.uid, column })
            .then(() => BoardStore.request(BoardStore.data.board.uid))
            .catch(() => alert('Неизвестная ошибка'))
            .then(this.context.setLoading(false))
    }

    render() {
        let {
            data: { id, name }
        } = this.props

        return (
            <Element>
                <Title>{name}</Title>
                <Menu>
                    <Icon src={trashIcon} size={14} onClick={() => this.handleDelete(id)} />
                    <Icon
                        src={editIcon}
                        size={14}
                        onClick={() =>
                            PopupStore.open(<ColumnEditPopup data={{ uid: BoardStore.data.board.uid, id, name }} />)
                        }
                    />
                    <Icon
                        src={plusIcon}
                        size={14}
                        onClick={() => PopupStore.open(<CardAddPopup data={{ id, name }} />)}
                    />
                </Menu>
            </Element>
        )
    }
}
