export const Element = styled.div`
    position: sticky;
    top: 0;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding: 30px;
    background: #eef0f4;
    z-index: 3;
`

export const Title = styled.b`
    font-weight: 400;
    font-size: 16px;
    opacity: 0.5;
`

export const Menu = styled.div`
    display: flex;
    align-items: center;
`

export const Icon = styled(UISvgIcon)`
    fill: ${rgba(COLOR.SILVER, 0.5)};
    margin-right: 10px;
    cursor: pointer;
    transition: fill 0.3s ease;

    :last-child {
        margin-right: 0;
    }

    :hover {
        fill: ${COLOR.BLACK};
    }
`
