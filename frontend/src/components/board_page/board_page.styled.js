export const Element = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: calc(100% - 100px);
    margin: 50px auto 0;
    min-height: calc(100vh - 100px);
`
