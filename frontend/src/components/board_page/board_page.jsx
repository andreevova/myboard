import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import Header from './header'
import Wrapper from './wrapper'
import { Element } from './board_page.styled'
import { BoardStore } from 'stores'

@observer
export default class BoardPage extends React.Component {
    state = BoardStore

    static childContextTypes = {
        setLoading: PropTypes.func.isRequired
    }

    getChildContext() {
        let { setLoading } = this
        return { setLoading }
    }

    setLoading = bool => this.state.set({ loading: bool === undefined ? !this.state.loading : bool })

    UNSAFE_componentWillMount() {
        document.title = this.state.data.board.name
    }

    componentWillUnmount() {
        this.state.reset()
    }

    onDragEnd = result => {
        const { source, destination } = result

        if (destination) {
            this.setLoading(true)
            if (source.droppableId === 'indexBoard' || destination.droppableId === 'indexBoard') {
                BoardStore.transferColumn(source, destination).then(this.setLoading(false))
            } else {
                BoardStore.transferCard(source, destination).then(this.setLoading(false))
            }
        }
    }

    render() {
        let { loading, data } = this.state

        return (
            <Element>
                <Header data={data.board} loading={loading} />
                {!!data.columns.length && (
                    <DragDropContext onDragEnd={this.onDragEnd}>
                        <Droppable droppableId="indexBoard" direction="horizontal">
                            {provided => <Wrapper data={data.columns} providedBoard={provided} />}
                        </Droppable>
                    </DragDropContext>
                )}
                <UIPopup />
            </Element>
        )
    }
}
