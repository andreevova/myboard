export default class AbstractState {
    constructor(props = {}) {
        this.set(props)
    }

    @action
    set(props) {
        Object.assign(this, props)
        return this
    }
}
