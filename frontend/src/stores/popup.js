class PopupStore {
    @observable content = []

    @action
    open = (content, size) => this.content.push({ size, content })

    @action
    close = () => this.content.splice(this.content.indexOf(this.content.length), 1)

    @computed
    get popups() {
        return this.content
    }
}

export default new PopupStore()
