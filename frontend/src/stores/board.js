class BoardStore extends AbstractState {
    @observable loading = false
    @observable data = { board: {}, columns: [] }

    @action
    reset() {
        this.data = { board: {}, columns: [] }
    }

    @action
    request = uid => {
        this.loading = true

        return new Promise((resolve, reject) => {
            Promise.all([
                Api.board('/check', { board: uid }),
                Api.column('/list', { board: uid }),
                Api.card('/list', { board: uid })
            ])
                .then(result => {
                    let [dataBoard, dataColumn, dataCard] = result
                    let { uid, name } = dataBoard
                    let newArrayColumns = []

                    if (localStorage.getItem('boards')) {
                        let boards = JSON.parse(localStorage.getItem('boards'))
                        let indexBoard = boards.findIndex(x => x.uid === uid)

                        if (indexBoard >= 0) boards[indexBoard].name = name
                        else boards.push({ uid, name })

                        localStorage.setItem('boards', JSON.stringify(boards))
                    } else {
                        localStorage.setItem('boards', JSON.stringify([{ uid, name }]))
                    }

                    dataColumn.map(column => {
                        let cards = dataCard.filter(card => card.column === column.id)
                        newArrayColumns.push({ ...column, cards })
                    })

                    this.data.board = dataBoard
                    this.data.columns = newArrayColumns
                    resolve(true)
                })
                .catch(err => reject(err))
                .then(() => (this.loading = false))
        })
    }

    @action
    transferCard({ droppableId: fromId, index: fromIndex }, { droppableId: toId, index: toIndex }) {
        const fromIndexColumn = this.data.columns.findIndex(x => x.id === fromId)
        const toIndexColumn = this.data.columns.findIndex(x => x.id === toId)
        const from = this.data.columns[fromIndexColumn].cards
        const to = this.data.columns[toIndexColumn].cards

        const [removed] = from.splice(fromIndex, 1)
        to.splice(toIndex, 0, removed)

        this.data.columns[fromIndexColumn].cards = from.filter(x => x !== undefined && x !== null)
        this.data.columns[toIndexColumn].cards = to

        return Api.card('/transfer', {
            board: this.data.board.uid,
            fromColumn: fromId,
            toColumn: toId,
            toId: to[toIndex].id,
            fromIndex,
            toIndex
        })
    }

    @action
    transferColumn({ index: fromIndex }, { index: toIndex }) {
        const to = this.data.columns
        const column = to[fromIndex].id

        const [removed] = to.splice(fromIndex, 1)
        to.splice(toIndex, 0, removed)

        this.data.columns = to.filter(x => x !== undefined && x !== null)

        return Api.column('/transfer', { board: this.data.board.uid, column, fromIndex, toIndex })
    }
}

export default new BoardStore()
