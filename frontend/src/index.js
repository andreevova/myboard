import { BrowserRouter } from 'react-router-dom'
import App from './app'

moment.locale('ru')

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById('app')
)
